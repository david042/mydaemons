#!/bin/bash

controle=0

echo $'o que deseja iniciar?\n'

function impressora {
	daemon1=$(systemctl is-active avahi-daemon)	
	daemon2=$(systemctl is-active cups)
	daemon3=$(systemctl is-active ecbd)
	if [ $daemon1 == "inactive" ]; then
		sudo systemctl start avahi-daemon
	fi
	if [ $daemon1 == "active" ]; then
		sudo systemctl stop avahi-daemon
	fi
	if [ $daemon2 == "inactive" ]; then
		sudo systemctl start cups
	fi
	if [ $daemon2 == "active" ]; then
		sudo systemctl stop cups
	fi
	if [ $daemon3 == "inactive" ]; then
		sudo systemctl start ecbd
	fi
	if [ $daemon3 == "active" ]; then
		sudo systemctl stop ecbd
	fi
}

function apache {
	daemon4=$(systemctl is-active mariadb)
	daemon5=$(systemctl is-active php-fpm)
	daemon6=$(systemctl is-active httpd)
	if [ $daemon4 == "inactive" ]; then
		sudo systemctl start mariadb
	fi
	if [ $daemon4 == "active" ]; then
		sudo systemctl stop mariadb
	fi
	if [ $daemon5 == "inactive" ]; then
		sudo systemctl start php-fpm
	fi
	if [ $daemon5 == "active" ]; then
		sudo systemctl stop php-fpm
	fi
	if [ $daemon6 == "inactive" ]; then
		sudo systemctl start httpd
	fi
	if [ $daemon6 == "active" ]; then
		sudo systemctl stop httpd
	fi	
}

function checarAtivo {
	daemon1=$(systemctl is-active avahi-daemon)
        daemon2=$(systemctl is-active cups)
        daemon3=$(systemctl is-active ecbd)
	daemon4=$(systemctl is-active mariadb)
	daemon5=$(systemctl is-active php-fpm)
	daemon6=$(systemctl is-active httpd)
	if [ $daemon1 == "active" ] && [ $daemon2 == "active" ] && [ $daemon3 == "active" ]; then
		a="desativar"	
	fi
	if [ $daemon1 == "inactive" ] || [ $daemon2 == "inactive" ] || [ $daemon3 == "inactive" ]; then
		a="ativar"	
	fi
	if [ $daemon4 == "active" ] && [ $daemon5 == "active" ] && [ $daemon6 == "active" ]; then
		b="desativar"
	fi
	if [ $daemon4 == "inactive" ] || [ $daemon5 == "inactive" ] || [ $daemon6 == "inactive" ]; then
		b="ativar"
	fi
}

while [ $controle -ne 1 ]; do
	clear
	checarAtivo
	echo -e "a - $a daemons de impressora\nb - $b daemons do apache\nc - sair"
	read -p $'a/b/c\x0a' opcao
	case $opcao in
		a)
		impressora;;
		b)
		apache;;
		c)
		controle=1;;
	esac
done
